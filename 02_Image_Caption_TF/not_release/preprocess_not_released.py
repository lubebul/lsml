import _pickle as cPickle
import re
import json
import numpy as np
import pandas as pd

"""
1. split train/test for the contest
2. process groundtruth captions for test set => test_gt.csv
"""

def extract_img_cap(fin, capmap):
    with open(fin) as json_data:
        d = json.load(json_data)
    idmap = {}
    for img in d['images']:
        idmap[img['id']] = img['file_name']
    capmap = {} if capmap is None else capmap
    for cap in d['annotations']:
        if idmap[cap['image_id']] not in capmap.keys():
            capmap[idmap[cap['image_id']]] = [cap['caption']]
        else:
            x = capmap[idmap[cap['image_id']]]
            x.append(cap['caption'])
            capmap[idmap[cap['image_id']]] = x
    return capmap
def extract_vocab_lower_caps(f):
    rst = {}
    vocab = set([])
    with open(f) as json_data:
        d = json.load(json_data)
    for img_id, caps in d.items():
        kname = '{}.jpg'.format(int(img_id.split('.')[0].split('_')[-1]))
        new_caps = []
        for cap in caps:
            cur_cap = []
            for w in re.split('(\w+)', cap):
                w = w.lower()
                if not w.isalpha():
                    continue
                vocab.add(w)
                cur_cap.append(w)
            new_caps.append(' '.join(cur_cap))
        rst[kname] = new_caps
    rst_name = 'processed.json'
    with open(rst_name, 'w') as outfile:
        json.dump(rst, outfile)
    print(len(vocab))
    cPickle.dump(list(vocab), open('vocab.pkl', 'wb'))


capmap = None
for fin in ['annotations/captions_train2014.json', 'annotations/captions_val2014.json']:
    print('processing {}'.format(fin))
    capmap = extract_img_cap(fin, capmap)
with open('cap_all.json', 'w') as outfile:
    json.dump(capmap, outfile)
extract_vocab_lower_caps('cap_all.json')

with open('processed.json') as json_data:
    capmap = json.load(json_data)
# dump to disk
N = len(capmap)
keys = list(capmap.keys())
ids = np.array(range(N))
np.random.shuffle(ids)
df_train = {}
df_test = {}
train = ids[:int(N*5/6)]
for i in range(N):
    if i in train:
        df_train[keys[i]] = capmap[keys[i]]
    else:
        df_test[keys[i]] = capmap[keys[i]]
with open('cap_train.json', 'w') as outfile:
    json.dump(df_train, outfile)
with open('cap_test.json', 'w') as outfile:
    json.dump(df_test, outfile)
# split test img/caption
with open('cap_test.json') as fin:
    df_test = json.load(fin)
imgs = list(df_test.keys())
df = pd.DataFrame({'img_id':imgs})
df.to_csv('test.csv', index=False)
# convert to csv file
with open('cap_train.json') as fin:
    df_train = json.load(fin)
img_id, caps = [], []
for k, vs in df_train.items():
    for v in vs:
        img_id.append(k)
        caps.append(v)
df_train = pd.DataFrame({'img_id':img_id, 'caption':caps})
df_train.set_index(['img_id'], inplace=True)
df_train.to_csv('train.csv')
with open('cap_test.json') as fin:
    df_test = json.load(fin)
img_id, caps = [], []
for k, vs in df_test.items():
    for v in vs:
        img_id.append(k)
        caps.append(v)
df_test = pd.DataFrame({'img_id':img_id, 'caption':caps})
df_test.set_index(['img_id'], inplace=True)
df_test.to_csv('test_gt.csv')