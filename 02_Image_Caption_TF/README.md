# not released files => only for preparation of the contest
 * not_release/ - contains scripts of [train/test splits of the contest] and the [preparation of scoring executable files]
 * image/ - contains all MSCOCO image files (since students have the preprocessed .pkl files)
 * run.py - contains scripts to generate TA80 benchamrk

# contest files [note: starter tutorial is merged with benchmark code for demo]
 * preprocess.ipynb - contains scripts of [preprocess of image, test for the contest]
 * ImageCaption.ipynb - merges starter tutorial and TA80 benchmark code
 * dataset/ - except image/
 * pre_trained/ - contains pre-trained models
 * CIDErD/ - contains scoring executables

Author: Ruochun Tzeng 2017
